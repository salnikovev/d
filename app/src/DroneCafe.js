'use strict';

var droneCafe = angular.module('droneCafe', [
  'ui.router',
  'ngRoute', 
  'restangular', 
  'ngMaterial'
  ]);

droneCafe.config(function config(
    $locationProvider, 
    RestangularProvider, 
    $stateProvider, 
    $urlRouterProvider
  ) {

    $urlRouterProvider.otherwise('/');

    RestangularProvider.setBaseUrl('/v1/data/');

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    $stateProvider.
      state('login', {
        url: '/login',
        component: 'login'
      }).
      state('logout', {
        url: '/logout',
        component: 'logout'
      }).
      state('myorder', {
        url: '/',
        component: 'myOrder'
      }).
      state('menu', {
        url: '/menu',
        component: 'menuOfDishes'
      }).
      state('kitchen', {
        url: '/kitchen',
        component: 'kitchen'
      });
  });

droneCafe
  .factory('mySocket', function(socketFactory) {
    //var myIoSocket = io.connect('https://netology-socket-io.herokuapp.com/');
    var myIoSocket = io.connect();
    mySocket = socketFactory({
      ioSocket: myIoSocket
    });
    mySocket.forward('changeStateDish');
    return mySocket;
  });
