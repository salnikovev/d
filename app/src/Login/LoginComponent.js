'use strict';

droneCafe.component('login', {

    controller: function loginCtrl(authService, $state) {
      var self = this;
      this.user = null;

      this.checkUser = user => {
        authService.login(user)
        .then(res => {
          authService.setUserId(res._id);
          $state.go('myorder');
        })
        .catch(error => console.log(error));
      };
    },

    templateUrl: './src/Login/Login.html'

  });
