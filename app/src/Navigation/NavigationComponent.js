'use strict';

droneCafe.component('navigation', {

  controller: function navigationCtrl(authService) {
    this.isLogin = authService.isLogin();
  },

  templateUrl: './src/Navigation/Navigation.html'

});
