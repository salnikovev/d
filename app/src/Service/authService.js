'use strict';

droneCafe.factory('authService', (Restangular, $http) => {

    let _userId = undefined;
    let status = false;

    return {
      setUserId: function(userId) {
        _userId = userId;
        status = true;
      },
      isLogin: function() {
        return status;
      },      
      getUserId: function() {
        return _userId;
      },
      login: function(newUser) {
        return Restangular.all('login').post(newUser);
      },
    }

  });
