'use strict';

const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const dbDroneCafe = require('./server/db');
const cfg = require('./config');
const menu = require('./menu.json');


// required middleware for express
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.disable('x-powered-by');

// express http server
const server = require('http').createServer(app);
// start socket
//const io = require('./server/realTime').realTime(server);
// front-end
app.use(express.static(path.join(__dirname, 'app')));
// server api
app.use('/v1/data', require('./server/api'));
// server db
dbDroneCafe.connect(cfg.dbUri);
// loadmenu
dbDroneCafe.loadMenu(menu)
  .then(dishes => console.log('Import menu success'))
  .catch(err => console.log('Error with menu import:', err));

// error handling
app.all('*', (req, res) => res.sendStatus(404));

app.use((err, req, res, next) => {
    console.log(err);
    if (!res.headersSent) res.sendStatus(404);
});

// start server 
server.listen(cfg.port, () => console.log('listening port ' + cfg.port));
