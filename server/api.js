const express = require('express');
const app = module.exports = express();
const dbDroneCafe = require('./db');

app.post('/login', (req, res) => {
  const { name, email } = req.body;
  console.log('Try login with: ', req.body);

  dbDroneCafe.userLogin(name, email)
    .then(user => res.status(200).json(user))
    .catch(err => res.sendStatus(500));
    
});
