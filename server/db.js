'use strict';

const mongoose = require('mongoose');
const { Dish, User, Order } = require('./dbModels');

function connect (DB) {
  mongoose.Promise = global.Promise;
  mongoose.connect(DB);

  mongoose.connection.on('connected', () => console.log('Mongoose default connection open to ' + DB));
  mongoose.connection.on('error', err => console.log('Mongoose default connection error: ' + err));
  mongoose.connection.on('disconnected', () => console.log('Mongoose default connection disconnected'));

  process.on('SIGINT', () => {
    mongoose.connection.close(() => {
      console.log('Mongoose default connection disconnected through app termination');
      process.exit(0);
    });
  });
}

function loadMenu (menu) {
  let menuPromises = menu.map(item => {
    return Dish.findOne({ name: item.title })
    .then(dish => {
      if (dish) {
        return dish;
      } else {
        dish = new Dish({
          name: item.title,
          id: item.id,
          image: item.image,
          ingredients: item.ingredients,
          price: item.price,
        });
        return dish.save();
      }
    })
  });
  return Promise.all(menuPromises);
}

function userLogin (name, email) {
  return new Promise(function(resolve, reject) {
    User.findOne({ email })
    .then(user => {
      if (!user) {
        user = new User({ name, email });
        user.save();
      };
      resolve(user);
    });
  });
}

module.exports = {
  connect,
  loadMenu,
  userLogin
}
